import datetime
from database import db


class TodoTasks(db.Model):
    __tablename__ = "todo_task"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), unique=True, nullable=False)
    done = db.Column(db.Boolean, default=False)
    created = db.Column(
        db.DateTime,
        nullable=True,
        default=datetime.datetime.utcnow,
    )
    due = db.Column(
        db.DateTime,
        nullable=True,
    )
    todo_list_id = db.Column(
        db.Integer,
        db.ForeignKey("todo_list.id"),
        nullable=False,
    )
    todo_list = db.relationship(
        "TodoList",
        backref=db.backref("todos", lazy=True)
    )
