from flask import Flask
from database import db
from urls import todo_list_api
from flask_migrate import Migrate

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://postgres:postgres@postgres/postgres'
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False


migrate = Migrate(app, db)
db.init_app(app)


# @app.cli.command()
# def createdata():
#     for i in range(10):
#         new_list = TodoList(name=f"New list {i}")
#         db.session.add(new_list)
#     db.session.commit()
#

app.register_blueprint(todo_list_api)

if __name__ == '__main__':
    app.run(host="0.0.0.0", port=8888, debug=True)
