import json
from flask.views import MethodView
from models.todo_list import TodoList
from database import db
from flask import request, abort


class TodoListCreateView(MethodView):
    def get(self, **kwargs):
        todo_lists = TodoList.query.all()
        return json.dumps(
            [todo_list.serialize() for todo_list in todo_lists]
        )

    def post(self, **kwargs):
        data_json = request.data
        data = json.loads(data_json)
        new_todo_list = TodoList(
            name=data.get('name'),
        )
        db.session.add(new_todo_list)
        db.session.commit()
        return json.dumps(new_todo_list.serialize())


class GetDeletePutTodoListView(MethodView):
    def get(self, **kwargs):
        todo_list_id = kwargs.get('id')
        todo_list = TodoList.query.filter_by(id=todo_list_id).first()
        if not todo_list:
            return abort(400, f'TodoList with ID {todo_list_id} does not exist!')
        return json.dumps(todo_list.serialize())

    def delete(self, **kwargs):
        todo_list_id = kwargs.get('id')
        todo_list = TodoList.query.filter_by(id=todo_list_id).first()
        if not todo_list:
            return abort(400, f'TodoList with ID {todo_list_id} does not exist!')
        db.session.delete(todo_list)
        db.session.commit()
        return json.dumps({
            "message": f"Todolist {todo_list_id} deleted"
        })

    def put(self, **kwargs):
        todo_list_id = kwargs.get('id')
        todo_list = TodoList.query.filter_by(id=todo_list_id).first()
        data_json = request.data
        data = json.loads(data_json)
        name = data.get('name')
        if not todo_list:
            return abort(400, f'TodoList with ID {todo_list_id} does not exist!')
        todo_list.name = name
        db.session.commit()
        return json.dumps({
            "message": f"Todolist {todo_list_id} updated"
        })


class SearchTodoListView(MethodView):
    def get(self, **kwargs):
        search_string = request.values.get('q')
        find = TodoList.query.filter_by(name=search_string).first()
        return json.dumps(find.serialize())

