from flask import Blueprint
from views.todo_list import TodoListCreateView
from views.todo_list import GetDeletePutTodoListView
from views.todo_list import SearchTodoListView

todo_list_api = Blueprint("todo-list-api", "todo-list-api")


todo_list_api.add_url_rule(
    rule="/todo-lists/",
    view_func=TodoListCreateView.as_view("todo-list-list-create"),
)
todo_list_api.add_url_rule(
    rule='/todo-lists/<int:id>/',
    view_func=GetDeletePutTodoListView.as_view('get-todo-list'),
)
todo_list_api.add_url_rule(
    rule='/todo-lists/search/',
    view_func=SearchTodoListView.as_view('todo-lists-search'),
)
