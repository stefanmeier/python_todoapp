FROM continuumio/miniconda:latest

#Make conda enviroment
COPY ./app/requirements.yml /app/requirements.yml
RUN /opt/conda/bin/conda env create -f /app/requirements.yml
ENV PATH /opt/conda/envs/app/bin:$PATH

#Copy python files
COPY ./app /app

COPY ./scripts/* /scripts/
RUN chmod +x /scripts/*

WORKDIR /app